.ONESHELL:

.DEFAULT_GOAL := run

PYTHON = venv/bin/python3
PIP = venv/bin/pip


venv/bin/activate: requirements.txt
#venv/Script/activate: requirements.txt
	python -m venv venv
	chmod +x venv/bin/activate
#takeown /f "venv/Script"	#for windows
	. ./venv/bin/activate
	$(PIP) install -r requirements.txt

venv: venv/bin/activate
	. ./venv/bin/activate

run: venv
	$(PYTHON) src/app.py

test: venv  
	. ./venv/bin/activate \
	&& $(PYTHON) -m pytest -v


clean:
	rm -rf src/__pycache__
	rm -rf venv

