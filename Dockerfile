FROM python:3.9-bookworm
LABEL Name="Python Atlanta Crime Analysis and Prediction" Version=1.0


ARG srcDir=src
WORKDIR /app
COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

COPY notebooks/atl_crime_notebook.ipynb .
#COPY $srcDir/utils.py .
#COPY $srcDir/ ./app
#
#RUN python3 /app/src/app.py