import utils as ut
import pandas as pd
import numpy as np
import nbformat as nbf
import os
from pathlib import Path


# def csv2dataframe(path):
workingdir = Path.cwd()
pathraw = workingdir.joinpath('data', 'raw')
pathcleaned = workingdir.joinpath('data', 'cleaned')
pathnotebook = workingdir.joinpath('notebooks')
notebookname = 'atl_crime_notebook.ipynb'
print(pathraw)
files = ut.getfilesnames(pathraw)

# clean old generated files if exists
if pathcleaned.joinpath('atlcrime.csv').exists():
    ut.cleanfiles(pathcleaned, 'atlcrime.csv')
else:
    pass

if pathnotebook.joinpath(notebookname).exists():
    ut.cleanfiles(pathnotebook, notebookname)
else:
    pass


data = ut.getvaliddataframe(pathraw)


# title cell
nb = nbf.v4.new_notebook()
title = """\
# Atlanta crime data analysis."""

import_code = """\
import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
import matplotlib.pyplot as plt
import os
import seaborn as sns
import folium
from folium.plugins import MarkerCluster # for clustering the markers
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from sklearn import metrics
# import fbprophet
from pathlib import Path
from scipy.stats import mode"""

# create library import cell
nb['cells'] = [nbf.v4.new_markdown_cell(title),

               nbf.v4.new_code_cell(import_code)]
fname = os.path.join(pathnotebook, notebookname)

# create read csv file cell
getdate_code = f"""#Chemin absolu vers le repertoire parent du fichier courant
working_dir = Path.cwd()
#chemin vers le fichier csv
csv_file = working_dir/".."/"data"/"cleaned"/"atlcrime.csv"
df = pd.read_csv(csv_file)
"""

nb["cells"] += [nbf.v4.new_code_cell(getdate_code)]

# create read dataframe cell
display_df_code = f"""df.head(5)"""

nb["cells"] += [nbf.v4.new_code_cell(display_df_code)]


# cleaning dataframe
info_clean = """\
We will clean  the dataframe here and convert the columns to the good data type
in order to make vizualisations."""
nb['cells'] += [nbf.v4.new_markdown_cell(info_clean)]

# create get info cell
getinfo_code = f"""df.info()"""

nb["cells"] += [nbf.v4.new_code_cell(getinfo_code)]

# create clean column's datatype cell
correct_datatypes_code = """\
df = df.astype({'beat' : 'int32', 'occur_year' : 'int32',
'occur_month' : 'int32', 'occur_day' : 'int32', 'occur_hour' : 'int32', 
'rpt_year' : 'int32', 'rpt_month' : 'int32', 'rpt_day' : 'int32'})
#delete wrong dates in the dataset
df['occur_date'] = pd.to_datetime(df['occur_date'], errors='coerce')
df['rpt_date'] = pd.to_datetime(df['rpt_date'], errors='coerce')
df = df.dropna(subset=['rpt_date'])
df = df.dropna(subset=['occur_date'])"""

nb["cells"] += [nbf.v4.new_code_cell(correct_datatypes_code)]


# create get info cell
getinfo_clean_code = f"""df.info()"""

nb["cells"] += [nbf.v4.new_code_cell(getinfo_clean_code)]

info_change_datatype = """\
We have changed the data types for the date columns."""
nb['cells'] += [nbf.v4.new_markdown_cell(info_change_datatype)]


info_EDA = """\
## Exploratory Data Analysis"""
nb['cells'] += [nbf.v4.new_markdown_cell(info_EDA)]


info_remove_nulls = """\
Find and remove Null values"""
nb['cells'] += [nbf.v4.new_markdown_cell(info_remove_nulls)]

# create clean null cell
get_nulls_code = f"""df.isnull().sum()"""

nb["cells"] += [nbf.v4.new_code_cell(get_nulls_code)]


crosstab = """\
Crosstab is important for EDA, useful for initial 'feel'."""
nb['cells'] += [nbf.v4.new_markdown_cell(crosstab)]


crosstab_code = f"""pd.crosstab(df['rpt_year'], df['UC2_Literal'])"""

nb["cells"] += [nbf.v4.new_code_cell(crosstab_code)]

graph_uc2_code = f"""
df.groupby(['rpt_year', 
'UC2_Literal']).size().unstack(level = 1).plot(figsize=(12,10))
"""

nb["cells"] += [nbf.v4.new_code_cell(graph_uc2_code)]

analyse_graph_uc2_code = """\
We can see that larceny is the highest wheter it is from vehicule or not."""
nb['cells'] += [nbf.v4.new_markdown_cell(analyse_graph_uc2_code)]

analyse_data_quality_per_year = """\
We analyse the data density per year."""
nb['cells'] += [nbf.v4.new_markdown_cell(analyse_data_quality_per_year)]

analyse_data_qual_code = f"""
df['occur_year'].value_counts().sort_index()
"""

nb["cells"] += [nbf.v4.new_code_cell(analyse_data_qual_code)]

analyse_data_result = """\
We can conclude that we can only work with data from 2009 onward. Which is done below"""
nb['cells'] += [nbf.v4.new_markdown_cell(analyse_data_result)]


limit_data_qual_code = f"""
df = df[(df['occur_year'] >= 2009) & (df['occur_year'] <= 2020)]
df['occur_year'].value_counts().sort_index()
"""

nb["cells"] += [nbf.v4.new_code_cell(limit_data_qual_code)]


convert_24_0 = """\
Convert in occur_hour 24 to 0."""
nb['cells'] += [nbf.v4.new_markdown_cell(convert_24_0)]


convert_24_0_code = f"""
def updateOccurHour(hour):
    if hour == 24:
        return 0
    else:
        return hour
df['occur_hour'] = df['occur_hour'].apply(updateOccurHour)
"""

nb["cells"] += [nbf.v4.new_code_cell(convert_24_0_code)]

analyse_with_graph = """\
###Use plot functions to analyse more the dataset."""
nb['cells'] += [nbf.v4.new_markdown_cell(analyse_with_graph)]

stack_graph_code = f"""
df.groupby(['occur_year', 'UC2_Literal']).size().unstack(level=1).plot.bar(stacked=True)
"""

nb["cells"] += [nbf.v4.new_code_cell(stack_graph_code)]

hbar_graph_code = f"""
df.groupby(['occur_year', 'UC2_Literal']).size().unstack(level=0).plot.barh(stacked=True)"""

nb["cells"] += [nbf.v4.new_code_cell(hbar_graph_code)]


hist_graph_code = f"""
df.groupby(['occur_year', 'UC2_Literal']).size().unstack(level=0).plot.hist(stacked=True, bins=20)"""

nb["cells"] += [nbf.v4.new_code_cell(hist_graph_code)]


box_graph_code = f"""
df.groupby(['occur_year', 'UC2_Literal']).size().unstack(level=0).plot.box(vert=False, sym='r+')"""

nb["cells"] += [nbf.v4.new_code_cell(box_graph_code)]

line_graph_code = f"""
df.groupby(['occur_year', 'UC2_Literal']).size().unstack(level=1).plot.area()
"""

nb["cells"] += [nbf.v4.new_code_cell(line_graph_code)]


pie_graph_code = f"""
df[df['occur_year'] == 2020].groupby(['occur_year', 'UC2_Literal']).size().plot.pie()"""

nb["cells"] += [nbf.v4.new_code_cell(pie_graph_code)]


analyse_number_incident = """\
Deploy number of incidents over years
"""
nb['cells'] += [nbf.v4.new_markdown_cell(analyse_number_incident)]

number_year_graph_code = f"""
df.groupby(['occur_year']).size().plot(figsize=(12,6))"""

nb["cells"] += [nbf.v4.new_code_cell(number_year_graph_code)]


analyse_ucl = """\
The above figure shows the number of incidents has been all the way going down.
\n
\n
analyse number by ucl litteral
"""
nb['cells'] += [nbf.v4.new_markdown_cell(analyse_ucl)]

ucl_graph_code = f"""
df.groupby(['occur_year', 'UC2_Literal']).size().unstack(level=1).plot(figsize=(12,6))
"""

nb["cells"] += [nbf.v4.new_code_cell(ucl_graph_code)]


analyse_month = """\
It looks like the number of incidents has been generally decreasing over years since 2009, which is a good sign of having a safer communities. However, one thing to notice is: 'LARCENY-FROM VEHICLE' incident has a slight increase trend. We need to pay attention to that.
\n
\n
\n
The number of crime over months.
"""
nb['cells'] += [nbf.v4.new_markdown_cell(analyse_month)]

monthly_graph_code = f"""
df.groupby(['occur_month']).size().plot(figsize=(12,6))
"""

nb["cells"] += [nbf.v4.new_code_cell(monthly_graph_code)]


result_analyse_month = """\
Febburary seems to be the safest month, it may bacause the crime-related ppl 
have a better financial status temporarily, by assuming they involve 
crimes due to financial cause. To make it cleaer, we can tell probably 
the stipend from government at the end of year can decrease the number of incidents.
"""
nb['cells'] += [nbf.v4.new_markdown_cell(result_analyse_month)]


monthly_ucl_graph_code = f"""
df.groupby(['occur_month', 'UC2_Literal']).size().unstack(level=1).plot(figsize=(12,6))
"""

nb["cells"] += [nbf.v4.new_code_cell(monthly_ucl_graph_code)]


analyse_hour = """\
Number of incidents over hours.
"""
nb['cells'] += [nbf.v4.new_markdown_cell(analyse_hour)]

hourly_graph_code = f"""
df.groupby(['occur_hour']).size().plot(figsize=(12,6))
"""

nb["cells"] += [nbf.v4.new_code_cell(hourly_graph_code)]


hourly_ucl_graph_code = f"""
df.groupby(['occur_hour', 'UC2_Literal']).size().unstack(level=1).plot(figsize=(12,6))
"""

nb["cells"] += [nbf.v4.new_code_cell(hourly_ucl_graph_code)]


result_analyse_hour = """\
A pick hour of crime would be around 8 and 12, so we can assume that is breakfast/lunch time. When they are out for food, then something happens.
"""
nb['cells'] += [nbf.v4.new_markdown_cell(result_analyse_hour)]

category_graph_code = f"""
ax = df.groupby(['UC2_Literal']).size().plot.bar(figsize=(12,6), title = '# of Incident Over Incident Category')
ax.set_xlabel("# of Incident")
ax.set_ylabel("Incident Category")"""

nb["cells"] += [nbf.v4.new_code_cell(category_graph_code)]


geolocal = """\
Geocoded location data for visualization."""
nb['cells'] += [nbf.v4.new_markdown_cell(geolocal)]


geolocal_graph_code = r"""
map = folium.Map(location=[df['lat'].mean(), df['long'].mean()], default_zoom_start=12)
# add a marker for every record in the filtered data, use a clustered view
marker_cluster = MarkerCluster().add_to(map) # create marker clusters
for i in range(500): # we can choose any number of incidents to plot on the map, eg: df.shape[0]
    location = [df['lat'][i],df['long'][i]]
    tooltip = "Neighborhood: {}<br> Click for more".format(df["neighborhood"][i])
    folium.Marker(location, 
                  popup=\"""<i>Crime Address: </i> <br> <b>{}</b> <br>\""".format(df['location'][i]), 
                  tooltip=tooltip).add_to(marker_cluster)
map.save('map.html')
map
"""

nb["cells"] += [nbf.v4.new_code_cell(geolocal_graph_code)]


machine_learning = """\
##4. Machine Learning"""
nb['cells'] += [nbf.v4.new_markdown_cell(machine_learning)]


linearreg = """\
We will split the dataset between training_data et test_data and train the model."""
nb['cells'] += [nbf.v4.new_markdown_cell(linearreg)]

linearreg_code = f"""
data = df.groupby(['occur_year']).size().reset_index()
X = data.iloc[:, :-1].values
y = data.iloc[:, 1].values
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)
reg = LinearRegression()
reg.fit(X_train, y_train)"""

nb["cells"] += [nbf.v4.new_code_cell(linearreg_code)]


result_linearreg_code = r"""
interc = reg.intercept_
coefc = reg.coef_
print(f"the intercept is : {interc}")
print(f"The coefficient is : {coefc}")"""

nb["cells"] += [nbf.v4.new_code_cell(result_linearreg_code)]


compare_code = r"""
y_pred = reg.predict(X_test)
r = pd.DataFrame({'Actual': y_test, 'Predicted': y_pred})
r"""

nb["cells"] += [nbf.v4.new_code_cell(compare_code)]


evaluate_model = """\
Evaluate the Algorithm.
"""
nb['cells'] += [nbf.v4.new_markdown_cell(evaluate_model)]


evaluate_model_code = r"""
print('Mean Absolute Error:', metrics.mean_absolute_error(y_test, y_pred))
print('Mean Squared Error:', metrics.mean_squared_error(y_test, y_pred))
print('Root Mean Squared Error:', np.sqrt(metrics.mean_squared_error(y_test, y_pred)))
r"""

nb["cells"] += [nbf.v4.new_code_cell(evaluate_model_code)]


visualize_model = """\
Evaluate the Algorithm.
"""
nb['cells'] += [nbf.v4.new_markdown_cell(visualize_model)]


visualize_model_code = r"""
data.plot(x='occur_year', y=0, style='o')
plt.plot(X_train, reg.predict(X_train), color = "r")
plt.title('# of Incidents Over Years')
plt.xlabel('Years')
plt.ylabel('# of Incidents')
plt.show()"""

nb["cells"] += [nbf.v4.new_code_cell(visualize_model_code)]


result_model_perf = """\
We can see that this model is pretty good at predicting.
"""
nb['cells'] += [nbf.v4.new_markdown_cell(result_model_perf)]

with open(fname, 'w') as f:
    nbf.write(nb, f)

# if __name__=="__main__":
#     # pass
#    csv2dataframe('..\\data\\raw')
