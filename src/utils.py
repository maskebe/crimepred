from csvvalidator import *
from re import search
from pathlib import Path
import pandas as pd


def getfilesnames(path: str):
    """This function return the path of the csv files
    The filename must contain the word "Cobra" 
    parameter: the absolute path to the directory containing the csv files
    returns: the list of accepted csv files"""
    goodfiles = []
    filenamecontains = "cobra"
    workingdir = Path.cwd()/'data'/'raw'

    filenames = workingdir.rglob("*")
    print(filenames)
    for filename in filenames:
        print(f'file name : {filename}')
        if search(filenamecontains, str(filename).lower()):
            goodfiles.append(str(workingdir.joinpath(filename)))
        else:
            print(f'File name must contain the word \"cobra\"')
    print(
        f'These files have been processed and accepted: {goodfiles}')
    return goodfiles


def cleanfiles(path, filename):
    """"This method deletes files constructed from the app.py file
    parameters: path to the file and the name of the file with the fileextension"""
    Path.unlink(path.joinpath(filename))


# def validatecsvfile(filename):
#     field_names = (
#         'offense_id',
#         'rpt_date',
#         'occur_date',
#         'occur_time',
#         'poss_date',
#         'poss_time',
#         'beat',
#         'location',
#         'UC2_Literal',
#         'neighborhood',
#         'npu',
#         'lat',
#         'long'
#     )
#     validator = CSVValidator(field_names)
#     validator.add_header_check(code='X1', message='error 1custom message')


def validatedataframe(filename, *nbrows: int):
    """This method reads a valide csv file as a pandas dataframe
    parameters: Absolute path to the file and the number of rows tyo process for big files.
    returns: Dataframe"""
    df = pd.read_csv(filename, low_memory=False)
    if (df.empty):
        print(f"CSV file is empty")
    else:
        return df


def getfilecolums(filename, *nbrows: int):
    """This method reads a valide csv file as a pandas dataframe to get the columns names
    parameters: Absolute path to the file and the number of rows tyo process for big files.
    returns: list of dataframe columns names"""
    df = validatedataframe(filename, nbrows)
    filecolumns = []
    for column in df.columns.values:
        filecolumns.append(column)
    return filecolumns


def checkmatchcols(filename, *nbrows: int):
    """This method reads a valide csv file as a pandas dataframe and transforms them to have 
    right columns names for the analysis
    parameters: Absolute path to the file and the number of rows tyo process for big files.
    returns: True or False"""
    df = validatedataframe(filename, nbrows)
    filecolumns = getfilecolums(filename, nbrows)
    realcolumns = ['offense_id', 'rpt_date', 'occur_date', 'occur_time', 'poss_date',
                   'poss_time', 'beat', 'location', 'UC2_Literal', 'neighborhood', 'npu', 'lat', 'long']
    if realcolumns.sort() == filecolumns.sort:
        # print(f"The csv file is valid!")
        return True
    else:
        # print(f"Load another csv file please!")
        return False


def cleancolums(filename, *nbrows: int):
    """This method reads a valide csv file as a pandas dataframe to clean the columns names and data types
    parameters: Absolute path to the file and the number of rows tyo process for big files.
    returns: Dataframe"""
    # data = validatedataframe(filename, nbrows)
    data = validatedataframe(filename)
    realcolumns = ['offense_id', 'rpt_date', 'occur_date', 'occur_time', 'poss_date',
                   'poss_time', 'beat', 'location', 'UC2_Literal', 'neighborhood', 'npu', 'lat', 'long']

    #
    if 'Apartment Office Prefix' in getfilecolums(filename):
        data = data.drop(columns=['Apartment Office Prefix', 'Apartment Number',
                                  'Shift Occurence', 'Location Type', 'UCR #', 'IBR Code'])
    elif 'apartment_office_prefix' in getfilecolums(filename):
        data = data.drop(columns=['apartment_office_prefix',
                                  'apartment_number', 'watch', 'location_type', 'UCR_Number'])
    elif 'ibr_code' in getfilecolums(filename):
        data = data.drop(columns=['ibr_code'])
    else:
        None
        print(f"CSV file is not recognised!")
    if checkmatchcols(filename, nbrows):
        pass
    else:
        data.columns = realcolumns
        data[["rpt_date", "occur_date", "poss_date"]] = data[[
            "rpt_date", "occur_date", "poss_date"]].apply(pd.to_datetime)
        return data


def checkdfcolumnstypes(filename, nbrows: int):
    """This method checks if the dataframe column types
    parameters: Absolute path to the file and the number of rows tyo process for big files.
    returns: None"""
    df = validatedataframe(filename, nbrows)
    for dtype in df.dtypes.items():
        print(dtype)


def enrichdataframe(data: pd.DataFrame):
    """This method reads a valid pandas dataframe to enrich it for visualisation
    parameters: dataframe.
    returns: Dataframe"""
    data['rpt_month'] = data['rpt_date'].dt.month
    data['rpt_day'] = data['rpt_date'].dt.day
    data['rpt_year'] = data['rpt_date'].dt.year
    data['occur_month'] = data['occur_date'].dt.month
    data['occur_day'] = data['occur_date'].dt.day
    data['occur_year'] = data['occur_date'].dt.year
    data['occur_hour'] = data['occur_time'].astype(str).str[:2].astype(int)

    return data


def dropnullvals(data: pd.DataFrame):
    """This method reads a valid pandas dataframe then drops null values
    parameters: dataframe.
    returns: Dataframe"""
    realcolumns = ['offense_id', 'rpt_date', 'occur_date', 'occur_time', 'poss_date',
                   'poss_time', 'beat', 'location', 'UC2_Literal', 'neighborhood', 'npu', 'lat', 'long']
    data = data.dropna(subset=realcolumns).reset_index(drop=True)

    return data


def getvaliddataframe(path, *nbrows: int):
    """This method reads a valide csv file as a pandas dataframe
    parameters: Absolute path to the file and the number of rows tyo process for big files.
    returns: Dataframe"""
    for file in getfilesnames(path):
        df = cleancolums(file)
        # df = checkdfcolumnstypes(file)
    # df = enrichdataframe(df)
    df = dropnullvals(df)
    return df


if __name__ == "__main__":
    # pass
    df = getvaliddataframe(
        r"E:\MasterAI2\versioning\project\crimepred\data\raw")
    # df = enrichdataframe(df)
    # df = dropnullvals(df)
    df.info()
